# NodeJS ORM

An Object-Relational-Mapping written in TypeScript for NodeJS. Fully compatible with JavaScript.

## Installation

You need to have NodeJS and NPM installed locally.

1. Run `npm install`
2. Create a database like `node_orm_test`
3. Change the database credentials inside `src/index.ts`
4. Transpile TypeScript to JavaScript via `npm run build`
5. Test the demo with `npm test`

## Other information

Created in correlation of a scientific paper in university.

### Licensed under the MIT license

For more information, read the [license](https://gitlab.com/tom7353/node-orm/blob/master/license) file.