export class StorageLayerError extends Error {

  constructor(message: string) {
    super(message);
  }

}