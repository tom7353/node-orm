export class NoPrimaryKeyError extends Error {

  constructor(message: string) {
    super(message);
  }

}