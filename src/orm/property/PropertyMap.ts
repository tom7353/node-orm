import { Types, Property } from './Property';

export interface PropertyMap {

  [uuid: string]: Array<Property<Types>>;

}