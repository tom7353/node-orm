import { IllegalArgumentError } from '../exception/IllegalArgumentError';
import { PropertyMap } from './PropertyMap';
import { BaseEntity } from '../entity/BaseEntity';

export type Types = number | string | boolean;

export class Property<TYPE extends Types> {

  private static valueMap: PropertyMap = {};

  private readonly entity: BaseEntity;
  private readonly name: string;
  private readonly type: string;
  private readonly nullable: boolean;
  private readonly primary: boolean;

  private value: TYPE | null;

  constructor(entity: BaseEntity, name: string, type: string, nullable: boolean, initialValue: TYPE | null = null, primary: boolean = false) {
    this.entity = entity;
    this.name = name;
    this.type = type;
    this.nullable = nullable;
    this.primary = primary;
    this.set(initialValue);
  }

  /** Gets the current value of this property */
  public get(): TYPE {
    return this.value;
  }

  /** Returns the entity this property belongs to */
  public getEntity(): BaseEntity {
    return this.entity;
  }

  /** Returns the name of this property */
  public getName(): string {
    return this.name;
  }

  /** Returns the type of this property as string */
  public getType(): string {
    return this.type;
  }

  /** Returns whether the property is the primary one */
  public isPrimary(): boolean {
    return this.primary;
  }


  /** Returns whether the property is nullable or not */
  public isNullable(): boolean {
    return this.nullable;
  }

  /**
   * Sets the current value of this property
   * @param value
   */
  public set(value: TYPE | null): void {
    if (value === null && !this.nullable) throw new IllegalArgumentError(this.name + ' is not nullable');
    this.value = value;
  }

  public static Id(parent: BaseEntity, name: string): Property<number> {
    return Property.createValue<number>(parent, name, 'number', true, null, true);
  }

  public static Number(parent: BaseEntity, name: string, nullable: boolean = true, initialValue: number | null = null): Property<number> {
    return Property.createValue<number>(parent, name, 'number', nullable, initialValue);
  }

  public static String(parent: BaseEntity, name: string, nullable: boolean = true, initialValue: string | null = null): Property<string> {
    return Property.createValue<string>(parent, name, 'string', nullable, initialValue);
  }

  public static Boolean(parent: BaseEntity, name: string, nullable: boolean = true, initialValue: boolean | null = null): Property<boolean> {
    return Property.createValue<boolean>(parent, name, 'boolean', nullable, initialValue);
  }

  private static createValue<VALUE_TYPE extends Types>(entity: BaseEntity, name: string, type: string, nullable: boolean = true, initialValue: VALUE_TYPE | null = null, primary: boolean = false): Property<VALUE_TYPE> {
    return Property.appendOrUpdateValueMap(entity.getUuid(), new Property<VALUE_TYPE>(entity, name, type, nullable, initialValue, primary));
  }

  private static appendOrUpdateValueMap<VALUE_TYPE extends Types>(uuid: string, value: Property<VALUE_TYPE>): Property<VALUE_TYPE> {
    if (!(uuid in Property.valueMap)) Property.valueMap[uuid] = [];

    let foundValue: Property<Types> | null = null;
    for (let candidate of Property.valueMap[uuid])
      if (candidate.getEntity().getTable() === value.getEntity().getTable() && candidate.getName() === value.getName()) {
        foundValue = candidate;
        break;
      }

    if (foundValue !== null)
      Property.valueMap[uuid].splice(Property.valueMap[uuid].indexOf(foundValue), 1);

    Property.valueMap[uuid].push(value);

    return value;
  }

  public static getPrimary(entity: BaseEntity): Property<number> | null {
    const uuid: string = entity.getUuid();
    if (uuid in Property.valueMap)
      for (let value of Property.valueMap[uuid])
        if (value.isPrimary())
          return value as Property<number>;
    return null;
  }

  public static getValues(entity: BaseEntity): Property<Types>[] {
    const uuid: string = entity.getUuid();
    if (uuid in Property.valueMap) return Property.valueMap[uuid];
    return [];
  }

}