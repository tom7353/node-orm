import { BaseEntity } from './entity/BaseEntity';
import { Property, Types } from './property/Property';

export class Debugger {

  public static printEntity(entity: BaseEntity, additionalTitle: string | null = null) {
    const values: Property<Types>[] = Property.getValues(entity);

    console.log('[', entity.getTable(), '#' + entity.fetchId(), '#' + entity.getUuid(), ']');

    if (additionalTitle != null)
      console.log('(' + additionalTitle + ')');

    for (let value of values)
      console.log('\'' + value.getName() + '\'', '(' + value.getType() + (value.isNullable() ? ', nullable)' : ')'), '=', value.get());
    console.log('');
  }

}