import { IllegalArgumentError } from '../exception/IllegalArgumentError';
import { Property } from '../property/Property';
import { NoPrimaryKeyError } from '../exception/NoPrimaryKeyError';

export class BaseEntity {

  private readonly uuid: string = 'e' + Math.random().toString(36).substr(2, 9);
  private readonly table: string;

  constructor(table: string | null = null) {
    if (this.table === null) throw new IllegalArgumentError('table cannot be null');
    this.table = table;
  }

  public fetchId(): number {
    const primary: Property<number> | null = Property.getPrimary(this);
    if (primary === null) throw new NoPrimaryKeyError(this.table + ' has no primary property declared');
    return primary.get() === null ? -1 : primary.get();
  }

  public getId(): Property<number> {
    const primary: Property<number> | null = Property.getPrimary(this);
    if (primary === null) throw new NoPrimaryKeyError(this.table + ' has no primary property declared');
    return primary;
  }

  public getUuid(): string {
    return this.uuid;
  }

  public getTable(): string {
    return this.table;
  }

}