import { StorageLayer } from './StorageLayer';
import { BaseEntity } from '../entity/BaseEntity';
import * as Mysql from 'mysql';
import { Connection, ConnectionConfig, MysqlError } from 'mysql';
import { StorageLayerError } from '../exception/StorageLayerError';
import { Property, Types } from '../property/Property';

export class MysqlStorage implements StorageLayer {

  private mysqlConnection: Connection | null = null;
  private readonly mysqlConnectionConfig: ConnectionConfig;

  constructor(user: string, password: string, database: string, host: string = 'localhost', port: number = 3306) {
    this.mysqlConnectionConfig = {
      host: host,
      port: port,
      database: database,
      user: user,
      password: password
    };
  }

  private static getSqlColumnType(type: string): string {
    switch (type) {
      case 'number':
        return 'int(11)';
      case 'string':
        return 'varchar(255)';
      case 'boolean':
        return 'tinyint(1)';
    }
    console.log('Missing type:', type);
    return 'varchar(15)';
  }

  private static createSqlQuery(sql: string, entity: BaseEntity, values: Property<Types>[] | null = null): string {

    if (sql.includes('tableName')) sql = sql.replace('tableName', entity.getTable());

    if (sql.includes('tableColumns')) sql = sql.replace('tableColumns', values.map((property: Property<Types>) => {
      if (property.isPrimary()) return property.getName() + ' int(11) auto_increment not null primary key';
      return property.getName() + ' ' + MysqlStorage.getSqlColumnType(property.getType()) + (property.isNullable() ? '' : ' not null');
    }).join(', '));

    if (sql.includes('insertProperties')) sql = sql.replace('insertProperties', values.filter(value => !value.isPrimary()).map(value => value.getName()).join(', '));

    if (sql.includes('insertValues')) sql = sql.replace('insertValues', values.filter(value => !value.isPrimary()).map(() => '?').join(', '));

    if (sql.includes('updateValues')) sql = sql.replace('updateValues', values.filter(value => !value.isPrimary()).map(value => value.getName() + ' = ?').join(', '));

    if (sql.includes('whereId')) sql = sql.replace('whereId', entity.getId().getName() + ' = ?');

    return sql;
  }

  private static createSqlValues(values: Property<Types>[]): any[] {
    return values.map(value => value.get());
  }

  private checkConnection(): Promise<Connection> {
    return new Promise<Connection>((resolve: (connection: Connection) => void, reject: () => void) => {

      if (this.mysqlConnection === null)
        this.connect()
          .then((connection: Connection) => resolve(connection))
          .catch(() => reject());

      else
        this.mysqlConnection.query('select 1;', (err: MysqlError | null) => {

          if (!err) return resolve(this.mysqlConnection);

          this.connect()
            .then((connection: Connection) => resolve(connection))
            .catch(() => reject());
        });
    });
  }

  private connect(): Promise<Connection> {
    return new Promise<Connection>((resolve: (connection: Connection) => void, reject: () => void) => {

      if (this.mysqlConnection) this.mysqlConnection.destroy();

      this.mysqlConnection = Mysql.createConnection(this.mysqlConnectionConfig);

      this.mysqlConnection.connect((err: MysqlError | null) => {
        if (err) return reject();
        resolve(this.mysqlConnection);
      });
    });
  }

  load<ENTITY extends BaseEntity>(entityType: { new(): ENTITY }, id: number): Promise<ENTITY> {
    return new Promise<ENTITY>((resolve: (entity: ENTITY) => void) => {
      this.checkConnection()
        .then((connection: Connection) => {

          const instance: ENTITY = new entityType();

          const mysqlCallback = (err: MysqlError | null, result: any) => {
            if (err) throw new StorageLayerError(err.sqlMessage);
            if (result.length != 1) throw new StorageLayerError('entity not found');

            const row: any = result[0];
            const values: Property<Types>[] = Property.getValues(instance);

            for (let value of values)
              if (value.getName() in row)
                value.set(row[value.getName()]);

            resolve(instance);
          };

          connection.query(
            MysqlStorage.createSqlQuery('select * from tableName where whereId;', instance),
            [id],
            mysqlCallback);

        })
        .catch(() => {
          throw new StorageLayerError('could not connect to mysql')
        });
    });
  }

  save(entity: BaseEntity): Promise<BaseEntity> {
    return new Promise<BaseEntity>((resolve: (entity: BaseEntity) => void) => {
      this.checkConnection()
        .then((connection: Connection) => {

          const values: Property<Types>[] = Property.getValues(entity);

          const mysqlCallback = (err: MysqlError | null, result: any) => {
            if (err) throw new StorageLayerError(err.sqlMessage);
            if (entity.fetchId() === -1)
              entity.getId().set(result.insertId);
            resolve(entity);
          };

          if (entity.fetchId() === -1)
            connection.query(
              MysqlStorage.createSqlQuery('insert into tableName (insertProperties) value (insertValues);', entity, values),
              MysqlStorage.createSqlValues(values.filter(value => !value.isPrimary())),
              mysqlCallback);
          else
            connection.query(
              MysqlStorage.createSqlQuery('update tableName set updateValues where whereId;', entity, values),
              MysqlStorage.createSqlValues(values.filter(value => !value.isPrimary())).concat([entity.fetchId()]),
              mysqlCallback);
        })
        .catch(() => {
          throw new StorageLayerError('could not connect to mysql')
        });
    });
  }

  createTable<ENTITY extends BaseEntity>(entityType: { new(): ENTITY }): Promise<void> {
    return new Promise<void>((resolve: () => void) => {
      this.checkConnection()
        .then((connection: Connection) => {

          const instance: ENTITY = new entityType();

          const values: Property<Types>[] = Property.getValues(instance);

          const mysqlCallback = (err: MysqlError | null) => {
            if (err) throw new StorageLayerError(err.sqlMessage);
            resolve();
          };

          connection.query(
            MysqlStorage.createSqlQuery('create table tableName (tableColumns);', instance, values),
            [],
            mysqlCallback
          );

        })
        .catch(() => {
          throw new StorageLayerError('could not connect to mysql')
        });
    });
  }

  dropTable<ENTITY extends BaseEntity>(entityType: { new(): ENTITY }): Promise<void> {
    return new Promise<void>((resolve: () => void) => {
      this.checkConnection()
        .then((connection: Connection) => {

          const instance: ENTITY = new entityType();

          const mysqlCallback = (err: MysqlError | null) => {
            if (err) throw new StorageLayerError(err.sqlMessage);
            resolve();
          };

          connection.query(
            MysqlStorage.createSqlQuery('drop table if exists tableName;', instance),
            [],
            mysqlCallback
          );

        })
        .catch(() => {
          throw new StorageLayerError('could not connect to mysql')
        });
    });
  }

}