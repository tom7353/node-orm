import { BaseEntity } from '../entity/BaseEntity';

export interface StorageLayer {

  load<ENTITY extends BaseEntity>(entityType: new () => ENTITY, id: number): Promise<ENTITY>;

  save(entity: BaseEntity): Promise<BaseEntity>;

}