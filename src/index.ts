import { ExampleEntity } from './example/entity/ExampleEntity';
import { Debugger } from './orm/Debugger';
import { MysqlStorage } from './orm/storage/MysqlStorage';
import { BaseEntity } from './orm/entity/BaseEntity';
import { performance } from 'perf_hooks';
import { Property } from './orm/property/Property';

console.log('[ NODE ORM TEST – Will it work? ]');
console.log('');

(async () => {
  try {

    const mysqlStorage: MysqlStorage = new MysqlStorage('root', '', 'node_orm');

    // Drop the table if it exists
    await mysqlStorage.dropTable(ExampleEntity);

    // Create the table
    await mysqlStorage.createTable(ExampleEntity);

    // Create an entity and set an example title
    const myExampleEntity: ExampleEntity = new ExampleEntity();
    myExampleEntity.title = 'some cool title ';

    // Print the constructed entity, -1 indicated that this entity has not been saved yet
    Debugger.printEntity(myExampleEntity, 'newly created entity');

    // Try to save the new entity
    let savedEntity: BaseEntity = await mysqlStorage.save(myExampleEntity);
    Debugger.printEntity(savedEntity, 'saved entity – the id should be different');

    // And now we try to load the exact same entity
    let loadedEntity: ExampleEntity = await mysqlStorage.load(ExampleEntity, savedEntity.fetchId());
    Debugger.printEntity(loadedEntity, 'loaded entity – everything should look the same');

    loadedEntity.title = 'changed title';
    Debugger.printEntity(loadedEntity, 'loaded entity with changes title');

    // update the entity
    let updatedEntity: BaseEntity = await mysqlStorage.save(loadedEntity);
    Debugger.printEntity(updatedEntity, 'updated entity');

    // test performance of the orm

    console.log('Testing performance...');

    // recreate table
    await mysqlStorage.dropTable(ExampleEntity);
    await mysqlStorage.createTable(ExampleEntity);

    await test('Mean insert time', async () => {
      let entity: ExampleEntity = new ExampleEntity();
      entity.title = 'inserted';
      performance.mark('start');
      await mysqlStorage.save(entity);
      performance.mark('end');
    });

    await test('Mean load time', async () => {
      let id: number = Math.round(Math.random() * 99) + 1;
      performance.mark('start');
      await mysqlStorage.load(ExampleEntity, id);
      performance.mark('end');
    });

    await test('Mean update time', async () => {
      let id: number = Math.round(Math.random() * 99) + 1;
      let entity: ExampleEntity = await mysqlStorage.load(ExampleEntity, id);
      entity.title = 'updated';
      performance.mark('start');
      await mysqlStorage.save(entity);
      performance.mark('end');
    });

    await test('Mean property initialization time', async () => {
      let entity: ExampleEntity = new ExampleEntity();
      performance.mark('start');
      Property.String(entity, 'id', true, 'created');
      performance.mark('end');
    });

    await test('Mean register read time', async () => {
      let entity: ExampleEntity = new ExampleEntity();
      entity.title = 'register';
      performance.mark('start');
      for (let value of Property.getValues(entity))
        value.get();
      performance.mark('end');
    });

    await test('Mean primary read time', async () => {
      let entity: ExampleEntity = new ExampleEntity();
      entity.title = 'register';
      performance.mark('start');
      Property.getPrimary(entity).get();
      performance.mark('end');
    });

    process.exit(0);
  } catch (e) {
    console.log(e);
  }
})();

async function test(name: string, callback: () => void) {
  let times: number[] = [];

  for (let i = 0; i < 100; i++) {
    performance.clearFunctions();
    performance.clearMeasures();
    performance.clearMarks();

    await callback();

    performance.measure('measure', 'start', 'end');
    times.push(performance.getEntriesByName('measure')[0].duration);
  }

  let meanTime: number = 0;
  times.map(v => meanTime += v);
  meanTime /= times.length;

  console.log(name, '(ms)', meanTime);
}