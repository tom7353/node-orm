import { Property } from '../../orm/property/Property';
import { BaseEntity } from '../../orm/entity/BaseEntity';

export class ExampleEntity extends BaseEntity {

  constructor() {
    super('example_entity');
  }

  private _id: Property<number> = Property.Id(this, 'id');

  private _title: Property<string> = Property.String(this, 'title', true);

  get id(): number {
    return this._id.get();
  }

  get title(): string {
    return this._title.get();
  }

  set title(value: string) {
    this._title.set(value);
  }

}