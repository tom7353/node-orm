const gulp = require('gulp');

const gulpTypescript = require('gulp-typescript');

const typescriptProject = gulpTypescript.createProject('tsconfig.json');

gulp.task('ts', () => typescriptProject.src()
  .pipe(typescriptProject())
  .on('error', console.log)
  .js
  .pipe(gulp.dest('build')));

gulp.task('watch', ['default'], () => {
  gulp.watch('src/**/*.ts', ['ts']);
});

gulp.task('default', ['ts']);